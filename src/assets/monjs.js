
function monQrCode() {
  var id = parseInt($( "#profilSelect" ).val());
  var motif = $( "#motifSelect" ).val();
 
  var mesProfils = JSON.parse(localStorage.getItem("mesProfils"));
  
  var monProfil = getProfilId(id, mesProfils);
  
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + "h" + Math.abs(today.getMinutes()-monProfil.retardHoraire) ;

  
  var heureSortie =today.getHours() + "h" + Math.abs(today.getMinutes()-(monProfil.retardHoraire-1)) ;
  

  var stringQrCode = "Cree le: "+date+ " a "+time+";%0A";
  stringQrCode += "Nom: "+monProfil.nom+";%0A";
  stringQrCode += "Prenom: "+ monProfil.prenom +";%0A";
  stringQrCode += "Naissance: " + monProfil.dateNaissance + " a "+monProfil.lieuNaissance +";%0A";
  stringQrCode += "Adresse: "+ monProfil.adresse +" "+ monProfil.cp + " "+ monProfil.ville +";%0A";
  stringQrCode += "Sortie: "+date +" a "+ heureSortie +";%0A"
  stringQrCode += "Motifs: "+ motif+";%0A"

  stringQrCode = stringQrCode.split(' ').join('%20');

  stringQrCode += "&size=700x700"


  var qrcodeUrl="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data="+stringQrCode;

  var domimg = "<img src ="+qrcodeUrl+"/>"

  var monElement = document.getElementById("img");
  monElement.innerHTML = '';

  var img = document.createElement('img');
  img.innerHTML = domimg.trim();

  monElement.appendChild(img.firstChild)

}




function getProfilId(id, array){
  var monProfil = null
  array.forEach(element => {
    if (element.id == id) 
    {
       monProfil = element;
    }
  });
  return monProfil;
}

    
  