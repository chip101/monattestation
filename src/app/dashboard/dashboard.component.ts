import { Component, OnInit } from '@angular/core';
import {ProfilService} from '../profil.service';
import {Profil} from '../models/interfaces';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {
  profils: Profil[] = [];

  constructor(private profilService: ProfilService) { }

  ngOnInit(): void {
    this.getProfils();
    this.profilService.save();
  }


  getProfils(): void {
    this.profilService.getProfils()
      .subscribe(profils => this.profils = profils);
  }

}
