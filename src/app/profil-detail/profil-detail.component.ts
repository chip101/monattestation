import {Component, Input, OnInit} from '@angular/core';
import {Profil} from '../models/interfaces';
import {ActivatedRoute} from '@angular/router';
import {ProfilService} from '../profil.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-profil-detail',
  templateUrl: './profil-detail.component.html',
  styleUrls: ['./profil-detail.component.css']
})
export class ProfilDetailComponent implements OnInit {

  @Input() profil: Profil;
  constructor(    private route: ActivatedRoute,
                  private profilService: ProfilService,
                  private location: Location) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.profilService.getProfil(id)
      .subscribe(profil => this.profil = profil);
  }

  goBack(): void {
    this.location.back();
  }
}
