import { Component, OnInit } from '@angular/core';
import {Profil} from '../models/interfaces';
import {ProfilService} from '../profil.service';
import {MessageService} from '../message.service';



@Component({
  selector: 'app-attestation',
  templateUrl: './profils.component.html',
  styleUrls: ['./profils.component.css']
})
export class ProfilsComponent implements OnInit {
  profils: Profil[];
  selectedProfil: Profil;
  constructor(private profilService: ProfilService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.profilService.getProfils().subscribe(profils => this.profils = profils);
  }

  onSelect(profil: Profil): void {
    this.messageService.add(`ProfilsComponent: Selected profil id=${profil.id}`);
    this.selectedProfil = profil;
  }

  public add (): void {
    this.profilService.add();
    
  }

  public remove(i):void {
   
    this.profilService.remove(i);

    this.profilService.getProfils().subscribe(profils => this.profils = profils);
  }

}
