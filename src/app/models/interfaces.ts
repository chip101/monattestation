export interface Profil {
  id: number;
  nom: string;
  prenom: string;
  lieuNaissance:string;
  dateNaissance:string;
  adresse:string;
  ville:string;
  cp:string;
  faitA:string;
  retardHoraire:number;


}
