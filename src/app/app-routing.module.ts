import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProfilsComponent} from './profils/profils.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ProfilDetailComponent} from './profil-detail/profil-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'profils', component: ProfilsComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: ProfilDetailComponent },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
