import { Injectable } from '@angular/core';
import {Profil} from './models/interfaces';
import {Observable, of} from 'rxjs';
import {MessageService} from './message.service';

var PROFILS: Profil[] = [
  { id: 1, nom: 'Pichon', prenom : "Nicolas", lieuNaissance : "Paris", dateNaissance:"1985-02-05", adresse:"5 chemin lilas", 
  ville:"bordeaux", cp:"33000", faitA:"Bordeaux", retardHoraire:25  },

  { id: 2, nom: 'DuPuy', prenom : "Michel", lieuNaissance : "Quimper", dateNaissance:"2002-07-03", adresse:"9 chemin ferry", 
  ville:"nantes", cp:"48000",  faitA:"nantes", retardHoraire:20 },
];

var dernierId = 2;




@Injectable({
  providedIn: 'root'
})

export class ProfilService {

  constructor(private messageService: MessageService) { }

  getProfils(): Observable<Profil[]> {
    this.messageService.add('ProfilService: fetched profil');
    return of(PROFILS);
  }

  getProfil(id: number): Observable<Profil> {
    this.messageService.add(`ProfilService: fetched profil id=${id}`);
    return of(PROFILS.find(profil => profil.id === id));
  }

  save():void
  {
    var profils = PROFILS;
    console.log("dans service " + profils);
    console.log("stringify " + JSON.stringify(profils));
    var mesProfils = JSON.stringify(profils);
    localStorage.setItem("mesProfils", mesProfils);
  }

  add():void
  {
    var id = dernierId+1;
    dernierId ++;
    var Profil = {id: id, nom: "/", prenom : "", lieuNaissance : "", dateNaissance:"", adresse:"", 
    ville:"", cp:"", faitA:"", retardHoraire:0  }
    PROFILS.push(Profil);

    this.save();
  }
  remove(id):void
  {
    var monProfil = PROFILS.find(profil => profil.id == id);
    
    PROFILS = PROFILS.filter(profil=> profil !== monProfil);

    this.save();
    
  }

}
