import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {  ProfilsComponent } from './profils/profils.component';
import {FormsModule} from '@angular/forms';
import { ProfilDetailComponent } from './profil-detail/profil-detail.component';

import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [
    AppComponent,
    ProfilsComponent,
    ProfilDetailComponent,
    DashboardComponent
  ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
